import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../../assets/style-reg.scss']
})
export class RegisterComponent{

  form;

  constructor(private auth: AuthService, private fb: FormBuilder)
  {
    this.form = fb.group({
      name: ['',Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

 
}
