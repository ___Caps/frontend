import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';  

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.css']
})
export class ConverterComponent implements OnInit {

  converter = {};
  converterId;
  constructor(private api: ApiService, private rout:ActivatedRoute){}
 
   ngOnInit(){
    this.converterId=this.rout.snapshot.paramMap.get('id');
     this.api.itemSelected.subscribe(c=>this.converter=c);
     this.api.getConvertor(this.converterId).subscribe(res => {
       this.converter=res;
      //  console.log(this.converter);
     });
   }
}
