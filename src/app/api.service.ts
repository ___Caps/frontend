import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private selectedItem = new Subject<any>();
  itemSelected = this.selectedItem.asObservable(); 
  

  constructor(private http: HttpClient ) {}
  getConvertors(){
    return this.http.get(`https://localhost:44307/api/home`);
  }
  getConvertor(convertorId){
    return this.http.get(`https://localhost:44307/api/home/${convertorId}`);
  }

  getCategories(){
    return this.http.get(`https://localhost:44307/api/home/categories`);
  }
  getSeries(){
    return this.http.get(`https://localhost:44307/api/home/series`);
  }
  getPower(){
    return this.http.get(`https://localhost:44307/api/home/power`);
  }
  getPhases(){
    return this.http.get(`https://localhost:44307/api/home/phases`);
  }
  getInitial(){
    return this.http.get(`https://localhost:44307/api/home`);
  }
  getFromFilter(filterstr){
    return this.http.get("https://localhost:44307/api/home/filter?"+ filterstr);
  }
  getOrders(userName){
    return this.http.get(`https://localhost:44307/api/user/order/${userName}`);
  }
  getRole(){
    return this.http.get<string>(`https://localhost:44307/api/account/role`);
  }
  getUserName(email){
    return this.http.get(`https://localhost:44307/api/account/name/${email}`);
  }

  deleteOrder(orderId) {
    return this.http.delete(`https://localhost:44307/api/admin/product/delete/${orderId}`);
  }
}
 
