import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';
import { ApiService } from '../api.service';

@Injectable()
export class AuthService {
  role: string;
  user : boolean;
  constructor(private http: HttpClient , private router: Router, private api: ApiService) { }
  
  get isAuthenticated(){
    return !!localStorage.getItem('token');
  }

  get isUser()
  {
    return !!localStorage.getItem('role');
  }
  get isAdmin()
  {
    return !!localStorage.getItem('Admin');
  }

  register(credentials){
    return this.http.post<any>(`https://localhost:44307/api/account/register`, credentials).subscribe(res=>{
      this.authenticate(res)
    });
  }

    
  login(credentials){
    return this.http.post<any>(`https://localhost:44307/api/account/login`, credentials).subscribe(res=>{
      this.authenticate(res)
    });
  }

    authenticate(res){
      localStorage.setItem('token', res)
      var decoded = jwt_decode(res);
      this.role = decoded.role;
      if(this.role == "Admin")
      {
        localStorage.setItem('Admin', "Admin");
        this.router.navigate(['/admin-board'])
      }
      else if(this.role == "User"){
        localStorage.setItem('role', "User");
        this.router.navigate(['/converters'])
      }
      else{
        console.log("Error Login")
      }
    }

    logout(){
      localStorage.removeItem('Admin');
      localStorage.removeItem('role');
      localStorage.removeItem('token');
      this.user = false;
      this.router.navigate(['/converters'])
    }


    canActivate (){
      var res = localStorage.getItem("token");
      var decoded = jwt_decode(res);
      this.role = decoded.role;
      console.log(this.role);
      if(this.role == "Admin")
      {
        return true;
      }
      else if (this.role == "User"){
        return false;
      }
      else {
        alert("badRole");
        console.log("bad role")
      }
    }
    

    userProfile()
    {
        this.router.navigate(['/my-profile'])
    }
    adminBoard(){
      this.router.navigate(['/admin-board'])
    }
}