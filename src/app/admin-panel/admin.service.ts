import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private selectedItem = new Subject<any>();
  itemSelected = this.selectedItem.asObservable(); 
  

  constructor(private http: HttpClient ) {}
  getUsers(){
    return this.http.get(`https://localhost:44307/api/admin`);
  }

  getUser(userId){
    return this.http.get(`https://localhost:44307/api/admin/user/${userId}`);
  }

  getProducts(){
    return this.http.get(`https://localhost:44307/api/admin/products`);
  }

  getProduct(productId){
    return this.http.get(`https://localhost:44307/api/admin/product/${productId}`);
  }

  deleteProduct(productId) {
    return this.http.delete(`https://localhost:44307/api/admin/product/delete/${productId}`);
  }

  deleteUser(userId) {
    return this.http.delete(`https://localhost:44307/api/admin/user/delete/${userId}`);
  }
  getUserName(email){
    return this.http.get(`https://localhost:44307/api/account/name/${email}`);
  }
}
 
