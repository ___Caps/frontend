import { Component, OnInit } from '@angular/core';
import { AdminService } from './admin.service';
import { ActivatedRoute} from '@angular/router'; 
import { Subject } from 'rxjs';

@Component({
  selector: 'user-modal',
  templateUrl: './user-details.html',
  styleUrls: ['./user-details.css']
})
export class UserModalComponent implements OnInit {

  public visible = false;
	private visibleAnimate = false;
	
  private selectedItem = new Subject<any>();
  itemSelected = this.selectedItem.asObservable(); 


	user = {};
  userId;
  constructor(private admin: AdminService, private rout: ActivatedRoute){}
 
  ngOnInit(){
  }

  public show(id): void {
    this.admin.itemSelected.subscribe(c=>this.user=c);
    this.admin.getUser(id).subscribe(res => {
      this.user=res;
    });

    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  public hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('modal')) {
      this.hide();
    }
  }
} 


