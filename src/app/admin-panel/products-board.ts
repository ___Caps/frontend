import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { PagerService } from '../pagination/pager.service'
import { AdminService } from './admin.service';
import jwt_decode from 'jwt-decode'

@Component({
  selector: 'product-panel',
  templateUrl: './products-board.html',
  styleUrls: ['./products-board.css']
})
export class AdminProductComponent implements OnInit {

constructor(private http: Http, private pagerService: PagerService, private adm: AdminService) { }

// array of all items to be paged
private allItems: any[];
admin
// pager object
pager: any = {};

// paged items
pagedItems: any[];

ngOnInit() {
   this.getAllProducts();
    this.show();
}

public show() : void{
  var res = localStorage.getItem('token');
  var decoded = jwt_decode(res);
  var email = decoded.name;
  this.adm.itemSelected.subscribe(c=>this.admin=c);
  this.adm.getUserName(email).subscribe(res => {
    this.admin=res;
  });
}
getAllProducts(){
    this.http.get(`https://localhost:44307/api/admin/products`)
    .map((response: Response) => response.json())
    .subscribe(data => {
        // set items to json response
        this.allItems = data;
        // initialize to page 1
        this.setPage(1);
    });
}
setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
}

deleteProduct(id: number){
    this.adm.deleteProduct(id).subscribe(data=>{
      this.getAllProducts();
    });
  }
    
}
