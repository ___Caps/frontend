import { Component, OnInit } from '@angular/core';
import { AdminService } from './admin.service';
import { ActivatedRoute} from '@angular/router'; 
import { Subject } from 'rxjs';

@Component({
  selector: 'product-modal',
  templateUrl: './product-details.html',
  styleUrls: ['./product-details.css']
})
export class ProductModalComponent implements OnInit {

  public visible = false;
	private visibleAnimate = false;
	
  private selectedItem = new Subject<any>();
  itemSelected = this.selectedItem.asObservable(); 


	product = {};
    productId;
  constructor(private admin: AdminService, private rout: ActivatedRoute){}
 
  ngOnInit(){
  }

  public show(id): void {
    this.admin.itemSelected.subscribe(c=>this.product=c);
    this.admin.getProduct(id).subscribe(res => {
      this.product=res;
      console.log(res);
    });

    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  public hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('modal')) {
      this.hide();
    }
  }
} 


