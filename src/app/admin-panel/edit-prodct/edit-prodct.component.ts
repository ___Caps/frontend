import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { ActivatedRoute} from '@angular/router'; 
import { Subject } from 'rxjs';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Component({
  selector: 'app-edit-prodct',
  templateUrl: './edit-prodct.component.html',
  styleUrls: ['./edit-prodct.component.css']
})
export class EditProdctComponent implements OnInit {

  public visible = false;
	private visibleAnimate = false;
	
  private selectedItem = new Subject<any>();
  itemSelected = this.selectedItem.asObservable(); 


	  product = {};
    productId;
    constructor(private admin: AdminService, private rout: ActivatedRoute,private http: Http){}

    ngOnInit(){

    }
    public show(id): void {
      this.admin.itemSelected.subscribe(c=>this.product=c);
      this.admin.getProduct(id).subscribe(res => {
        this.product=res;
        console.log(res);
      });
  
      this.visible = true;
      setTimeout(() => this.visibleAnimate = true, 100);
    }
  
    public hide(): void {
      this.visibleAnimate = false;
      setTimeout(() => this.visible = false, 300);
    }
  
    public onContainerClicked(event: MouseEvent): void {
      if ((<HTMLElement>event.target).classList.contains('modal')) {
        this.hide();
      }
    }
  //   test(p, pw,m,d){
  //     const auth_token = `Bearer ${localStorage.getItem('token')}`;
  // const headers = new Headers({
  //   'Content-Type': 'application/json',
  //   'Authorization': auth_token
  // });
  //     var bodyObj = {
  //       phases: p,
  //       power: pw,
  //       money: m,
  //       description:d
  //     }
  //     let url = `https://localhost:44307/api/user/product/count/`;
  //   this.http.put(url, bodyObj, {
  //   headers:headers
  // }).subscribe(res=>console.log(res));

  //   }
}
