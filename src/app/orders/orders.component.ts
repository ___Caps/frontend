import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
 orders;
  constructor(private api: ApiService) { }

  ngOnInit() {

    this.api.getConvertors().subscribe(res => {
      this.orders=res;
    });
  }

}
