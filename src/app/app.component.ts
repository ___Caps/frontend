import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
  // template : '<app-home></app-home>'
})
export class AppComponent {
  title = 'shop';
}
