import { Component, OnInit} from '@angular/core';
import { ApiService } from '../api.service';
import { Http, Headers, RequestOptions } from '@angular/http';
import { PagerService } from '../pagination/pager.service'
import { trigger, style, animate, transition } from '@angular/animations';
import { concat } from 'rxjs';

@Component({
  selector: 'app-converters',
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity:0}),
        animate(500, style({opacity:1})) 
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({opacity:0})) 
      ])
    ]),
    trigger('refreshFade', [
      transition(':enter', [  
        style({opacity:0}),
        animate(5000, style({opacity:1})) 
      ])
    ])
  ],
  templateUrl: './converters.component.html',
  styleUrls: ['./converters.component.css']
})

export class ConvertersComponent implements OnInit{
filters; products; temp;
url;
paged
cnt
cntpage;
 constructor(private api: ApiService, private http: Http, private pagerService: PagerService){}

 ngOnInit()
  {
    this.cntpage = 1;
    //this.getPagination();
    this.url = "";
    this.api.getInitial().subscribe(res=>
      {
        this.filters = res["filters"].reverse()
        this.products = res["products"]
        this.cnt = this.products.length;
      });
  }
  getFromFilter(filterId){
    var n = this.url.includes("&id="+filterId);
    console.log(n);
    if(n)
    {
      this.url = this.url.replace("&id="+filterId, '');
    }
    else
    {
      this.url = this.url+"&id="+filterId;
    }
    console.log(this.url)
    this.api.getFromFilter(this.url).subscribe(res =>{
      
        this.products = res["products"]
        this.cnt = this.products.length;
        
      });
  }

  getFromFilterPaged(page){
    var n = this.url.includes("&page="+page);
    if(n)
    {
    this.cntpage ++;
    this.url = this.url.replace("&page="+page, '');
    this.url = this.url+"&page="+ ++page;
    }
    else{
      this.cntpage ++;
      page++;
      this.url = this.url+"&page="+ page;
    }
    this.api.getFromFilter(this.url).subscribe(res =>{
      this.temp = res["products"]
      this.products = this.products.concat(this.temp)
      // concat(this.products, this.temp).subscribe(res=> {
      //   this.products = res;
      // });
        this.cnt = this.products.length;
      });
  }

  postData(productId){
    const auth_token = `Bearer ${localStorage.getItem('token')}`;
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': auth_token
    });
    let url = `https://localhost:44307/api/user/orderProduct/`;
    this.http.post(url, productId, {
      headers:headers
    }).subscribe(res=>console.log("ok"));
  }


}

