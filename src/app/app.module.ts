import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ApiService } from './api.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// import { MaterialAppModule } from './ngmaterial.module';
import { HomeComponent } from './home/home.component';
import { ConvertersComponent } from './converters/converters.component';
import { ConverterComponent } from './converter/converter.component';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatInputModule, MatCardModule, MatListModule, MatToolbarModule, MatFormFieldModule} from '@angular/material';
import { RegisterComponent } from './register/register.component';
import { AuthService } from './auth/auth.service';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AboutComponent } from './about/about.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AdminProductComponent } from './admin-panel/products-board';
import { AdminService } from './admin-panel/admin.service';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';

import { ModalComponent } from './modal.component';
import { UserModalComponent } from './admin-panel/user-details';
import { ProductModalComponent } from './admin-panel/product-details';

import { HttpModule } from '@angular/http';
import { PaginationComponent } from './pagination/pagination.component';
import { PagerService } from './pagination/pager.service';
import { OrdersComponent } from './orders/orders.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { EditProdctComponent } from './admin-panel/edit-prodct/edit-prodct.component';



const routes = [{ path: 'home', component: HomeComponent },
{ path: 'converters', component: ConvertersComponent },
{ path : 'converter/:id', component: ConverterComponent },
{ path: 'register', component: RegisterComponent }, 
{ path: 'about', component: AboutComponent },
{ path: 'admin-board', component: AdminPanelComponent, canActivate: [AuthService], data: {role: 'true'} },
{ path: 'product-board', component: AdminProductComponent, canActivate: [AuthService], data: {role: 'true'}},
{ path: 'login', component: LoginComponent },
{ path: 'order', component: OrdersComponent },
{ path: 'my-profile', component: UserProfileComponent, userProfile: [AuthService], data: {role: 'User'}}
];



@NgModule({
  declarations: [
    AppComponent, ModalComponent,
    HomeComponent,
    ConvertersComponent,
    ConverterComponent,
    RegisterComponent,
    AboutComponent,
    AdminPanelComponent,
    AdminProductComponent,
    NavbarComponent,
    LoginComponent,
    UserModalComponent, ProductModalComponent,
    PaginationComponent,
    OrdersComponent,
    UserProfileComponent,
    EditProdctComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    // MaterialAppModule,import { Ng2CarouselamosModule } from 'ng2-carouselamos';
    HttpClientModule, HttpModule,
    ReactiveFormsModule,FormsModule,
    MatButtonModule, MatInputModule, MatCardModule, MatListModule, MatToolbarModule, MatFormFieldModule
  ],
  providers: [ApiService, AdminService, PagerService, AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass : AuthInterceptor,
      multi: true
    }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
