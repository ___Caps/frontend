import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { PagerService } from '../pagination/pager.service'
import jwt_decode from 'jwt-decode';
import { Subject } from 'rxjs';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  private selectedItem = new Subject<any>();
  itemSelected = this.selectedItem.asObservable(); 
  name;
  email;
  private allItems: any[];
  pager: any = {};
  pagedItems: any[];
  countProduct;
  constructor(private http: Http, private pagerService: PagerService, private api: ApiService) { }

  ngOnInit() {
    this.getUserInfo();
    this.getOrders();
  }

getUserInfo(){
  var res = localStorage.getItem('token');
  var decoded = jwt_decode(res);
  this.email = decoded.name;
  this.api.itemSelected.subscribe(c=>this.name=c);
  this.api.getUserName(this.email).subscribe(res => {
    this.name=res;
  });
}
  getOrders(){

    const auth_token = `Bearer ${localStorage.getItem('token')}`;
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': auth_token
    });
    this.http.get(`https://localhost:44307/api/user/order`,{ headers: headers })
    .map((response: Response) => response.json())
    .subscribe(data => {
        this.allItems = data;
        this.setPage(1);
       
    });
}
setPage(page: number) {
  
 
    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);
    this.countProduct = this.allItems.length;
    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  
}

postCount(productId, count){
  const auth_token = `Bearer ${localStorage.getItem('token')}`;
  const headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': auth_token
  });
  var cnt =+count;
  let bodyObj = {
    productId: productId,
    count : cnt
  };
  console.log(bodyObj)
  let url = `https://localhost:44307/api/user/product/count/`;
  this.http.put(url, bodyObj, {
    headers:headers
  }).subscribe(res=>console.log(res));
}

deleteOrder(productId){
  const auth_token = `Bearer ${localStorage.getItem('token')}`;
  const options = {
    headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': auth_token
    }),
    body: {
      productId: productId
    },
  };
  let url = `https://localhost:44307/api/user/order/`;
  this.http.delete(url, options).subscribe(
    res=>{console.log(res)
    this.getOrders()}
    );
  }
}